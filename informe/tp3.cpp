


///Kamvar et al: Algoritmo 1
void algoritmo_1(Esparsa* P, double*& y, double* x, double c, int n){
	
	double w = norma1(x,n);
	
	P->mul_vector(y,x,n);
	vectorXEscalar(y,y,c,n);
	
	w -= norma1(y,n);
	
	vectorXEscalar(vec_aux,v,w,n);
	
	sumaVectorial(y,y,vec_aux,n);
	
}

///Kamvar et al: Algoritmo 2
double* algoritmo_2(Esparsa* P, double* x0, double c, int iters, int n){
	
	int i = 0;
	double* xn = new double[n];
	double* xm = new double[n];
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm,xn,c,n);
		algoritmo_1(P,xn,xm,c,n);
		
		restaVectorial(vec_aux,xn,xm,n);
		
		i+= 2;
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);
	
	///Normalizar el vector xn
	vectorXEscalar(xn,xn,1/norma1(xn,n),n);	
	
	delete[] xm;
	return xn;
	
}

///Calcula Extrapolacion Cuadratica
double * extrapolacionQuadratica(double * xm_3,double * xm_2,double * xm,double * xn, int n){
	
	///Inicializar yk's
	
	double* yk = new double[n];
	double* yk_1 = new double[n];
	double* yk_2 = new double[n];
	double * beta0xm_2 = new double[n];
	double * beta1xm = new double[n];
	double * beta2xn = new double[n];
	
	restaVectorial(yk_2,xm_2,xm_3,n);
	restaVectorial(yk_1,xm,xm_3,n);
	restaVectorial(yk,xn,xm_3,n); 
	
	///Inicializar Y
	
	double* Y = new double[2*n]; // Y tiene n filas y 2 columnas
	
	for (int i=0; i < n;i++) Y[i*2]=yk_2[i];
	for (int i=0; i < n;i++) Y[i*2+1]=yk_1[i];		
	
	///Vector Gamma y Beta
	
	double* gammas = new double[4];
	double* betas = new double[3];
	
	gammas[3] = 1;
	
	cuadradosMinimos(gammas,Y,yk,n);
	
	gammas[0]=-gammas[1]-gammas[2]-gammas[3];
	
	betas[0]=gammas[1]+gammas[2]+gammas[3];
	betas[1]=gammas[2]+gammas[3];
	betas[2]=gammas[3];
	
	vectorXEscalar(beta0xm_2,xm_2,betas[0],n);
	vectorXEscalar(beta1xm,xm,betas[1],n);
	vectorXEscalar(beta2xn,xn,betas[2],n);
	
	sumaVectorial(xn,beta0xm_2,beta1xm,n); 
	sumaVectorial(xn,xn,beta2xn,n);
	
	///Normalizar el vector xn
	vectorXEscalar(xn,xn,1/norma1(xn,n),n);
	
	delete[] yk;
	delete[] yk_1;
	delete[] yk_2;
	delete[] Y;
	delete[] gammas;
	delete[] betas;
	delete[] beta0xm_2;
	delete[] beta1xm;
	delete[] beta2xn;
	
	return xn;
}



void gramSchmidt(double * Q, double * R, double * A, int n){
	
	/**
	* Esta funcion esta implementada 
	* solamente para el caso donde 
	* A tiene n filas y 2 columnas
	*/
	
	///Inicializar Q y R

	ceros(Q,n,2);
	ceros(R,2,2);
	
	double * vec = new double [n];
	double * aux = new double  [n];
	double * Qaux = new double [n];
	
	///Obtener primera columna de A
	for (int i=0; i < n; i++){
		vec[i]=A[i*2];
	}
	
	R[0]=norma2(vec,n);
	copy(vec,vec+n,aux); ///aux=u1=v1
	vectorXEscalar(Qaux,vec,1/R[0],n);
	
	///Construir primera columna de Q
	for (int i=0; i < n; i++){
		Q[i*2]=Qaux[i];
	} 
	
	///Obtener segunda columna de A
	for (int i=0; i < n; i++){
		vec[i]=A[i*2+1];
	}
	
	R[1]=productoInterno(aux,vec,n);
	vectorXEscalar(aux,aux,pow(R[0],2),n);
	restaVectorial(vec,vec,aux,n);
	
	R[3]=norma2(vec,n);
	
	vectorXEscalar(aux,vec,1/R[3],n);
	///Construir segunda columna de Q
	for (int i=0; i < n; i++){
		Q[i*2+1]=aux[i];
	}	
	
	delete [] vec;
	delete [] aux;
	delete [] Qaux;
	
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P1 (P' Kamvar et al).
double * calcularP1(int * matrizCon,const int n){
    double * gradosPag = new double [n];
    
    for (int i=0; i < n; i++){
        gradosPag[i]=calcularGrado(matrizCon,n,i);
    }
    
    double * P1 = calcularP(matrizCon,n, gradosPag);
    
    double * d = new double[n];
    
    for(int i=0; i < n; i++){
        if (gradosPag[i]==0) d[i]=1;
        else d[i]=0;
    }
    
    double * D = new double[n*n];
    vectorXVectorTras(D,v,d,n);
    
    sumaMatricial(P1,P1,D,n);
    
    delete[] gradosPag;
    delete[] d;
    delete[] D;
    
    return P1;
}

///Calcula Cuadrados Minimos
void cuadradosMinimos(double * gammas,double * Y,double * yk,int n){
	double * Q = new double [2*n];
	double * R = new double [4];
	
	gramSchmidt(Q,R,Y,n); ///Obtener fact QR de Y
	
	matrizXEscalarMN(Q, -1, n,2); ///Q=-Q
	
	trasponerMatrizMN(Q,n,2); ///Q=Q'
	
	double Qty[2] ; ///obtengo -Q'*y
	productoMatricialMN(Qty, Q, yk, 2, n, n, 1);
	
	///backward para resolver R*(gammas[1] gammas[2])'=_Qty
	gammas[2]=Qty[1]/R[3];
	gammas[1]=(Qty[0]-R[1]*gammas[2])/R[0];
	
	delete [] Q;
	delete [] R;
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P2 (P'' Kamvar et al).
double * calcularP2(double * P1,const int n, const double c){
    double * unos = new double[n];
    
        for(int i=0; i < n; i++){
           unos[i]=1;
        }

    double * E = new double[n*n];
    double * P2 = new double[n*n];
    
    vectorXVectorTras(E,v,unos,n);
    
    matrizXEscalar(P1, c, n);
    matrizXEscalar(E, 1-c, n);
    
    sumaMatricial(P2,P1,E,n);
    
    delete[] unos;
    delete[] E;
    
    return P2;
    
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P (P Kamvar et al).
double * calcularP(int * matrizCon,const int n, double * gradosPag){
    double * P = new double [n*n];
    
    for(int i=0;i < n; i++)
        for(int j=0;j < n; j++){
            if (matrizCon[i*n+j]!=0) P[i*n+j] = 1/gradosPag[j];
            else P[i*n+j]=0;
        }
    return P;
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula el grado de cada nodo.
int calcularGrado(int * matrizCon,const int n,int pagina){
    int grado = 0;
    for (int i=0; i < n; i++){
        grado += matrizCon[i*n+pagina];
    }
    return grado;
}

