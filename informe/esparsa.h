class Esparsa{
	
	public:
	
		Esparsa(int n);
		~Esparsa();
		void relacion(int i, int j);
		void a_semi_estocastica();
		void mul_vector(double*& y, double* x, int n);
	
	private:
	
		int tam;
		ESPARSA* matriz;
		double* intermedio;
	
};
