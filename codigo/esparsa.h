
#ifndef ESPARSA_H
#define ESPARSA_H

#include <list>
#include <utility>
#include <iostream>
#include <stdio.h>

#define ESPARSA			std::list< std::pair<int, double> >
#define MENSAJE(m)		std::cout << m << std::endl
#define ITERADOR		std::list< std::pair<int, double> >::iterator
#define PAR(a,b)		std::pair<int,double>(a,b)

class Esparsa{
	
	public:
	
		Esparsa(int n);
		~Esparsa();
		void relacion(int i, int j);
		void a_semi_estocastica();
		void mul_vector(double*& y, double* x, int n);
	
	private:
	
		int tam;
		ESPARSA* matriz;
		double* intermedio;
	
};

#endif
