#ifndef UTILS_H_
#define UTILS_H_

#include <math.h>
#include <iostream>
#include <fstream>
#include "tiempo.h"

using namespace std;

void restaMatricial(double* res,double *a,double *b,int n);
void restaVectorial(double z[],double x[],double y[],int n);
void sumaVectorial(double z[],double x[],double y[],int n);
void sumaMatricial(double* res,double *a,double *b,int n);
void factorizacionQR(double *a, double *q, double *r, int n);
void vectorXVectorTras(double* res,double *z, double *w,int n);
void vectorXEscalar(double res[],double x[],double a,int n);
void vectorXMatriz(double* res,double *z, double *m, int n);
void swapear(double &a, double &b);
void matrizXEscalar(double *m, double alfa, int n);
void matrizXEscalarMN(double *a, double alfa, int n, int m);
void matrizXVector(double *m,double *x, double *b, int n);
void identidad(double *id,int k,int n);
void ceros(double *a,int m,int n);
void copiarMatriz(double *a, double *b, int n);
double norma1(double x[], int n);
double norma2(double x[], int n);
double productoInterno(double *v, double *w,int n);
void productoMatricialMN(double *axb, double *a, double *b, int m, int n1, int n2, int p);
void imprimirMatriz(double *m, int n);
void imprimirMatrizMN(double *a, int m, int n);
void imprimirVector(double *v,int n);
void guardarVector(const char* nombre, double *vec, int n);
void guardarVector(const char* nombre, unsigned long long int *vec, int n);
void guardarVector(const char* nombre, int *vec, int n);
void trasponerMatriz(double *a, int n);
void trasponerMatrizMN(double *a, int m, int n);
void conversor(const char* archivo_origen, const char* archivo_destino);
void ordenar(const char* nombre, double* autovector, int n);

#endif
