
#include "utils.h"

///Matriz X Escalar
void matrizXEscalar(double *m, double alfa, int n){
    	for (int i = 0; i<n; i++){
        	for (int j = 0; j<n; j++){
            		m[i*n+j]*=alfa;
        	}
    	}
}

///Matriz X Escalar matrices m x n
void matrizXEscalarMN(double *a, double alfa, int n, int m){
    	for (int i = 0; i<m; i++){
        	for (int j = 0; j<n; j++){
            		a[i*n+j]*=alfa;
        	}
    	}
}

///Matriz * vector
void matrizXVector(double *m,double *x, double *b, int n){
	double res;

	for (int i = 0; i<n; i++){
	    res = 0;
        for (int j = 0; j<n; j++){
            res += m[i*n+j] * x[j];
        }
        b[i] = res;
    }
}

///Matriz Traspuesta
void trasponerMatriz(double *a, int n){
    	for (int i = 0; i<n; i++){
        	for (int j = i+1; j<n; j++){
            		swapear(a[j*n+i],a[i*n+j]);
        	}
    	}
}

///Matriz Traspuesta mxn
void trasponerMatrizMN(double *a, int m, int n){
    	for (int i = 0; i<m; i++){
        	for (int j = 0; j<n; j++){
            		swapear(a[j*m+i],a[i*n+j]);
        	}
    	}
}


///Suma Matricial
void sumaMatricial(double* res,double *a,double *b,int n){
	for (int i = 0; i<n; i++){
		for (int j = 0; j<n; j++){
			res[i*n+j]=a[i*n+j]+b[i*n+j];
		}
	}
}

///Vector X Vector Traspuesto
void vectorXVectorTras(double* res,double *z, double *w,int n){
	for (int i = 0; i<n; i++){
		for (int j = 0; j<n; j++){
			res[i*n+j]=z[i]*w[j];
		}
	}
}

///Vector X Escalar
void vectorXEscalar(double res[],double x[],double a,int n){
	for (int i = 0; i<n; i++){
		res[i]=a*x[i];
	}
}

///Norma 1
double norma1(double x[], int n){
	double suma=0;
	for (int i = 0; i<n; i++){
		suma+=fabs(x[i]);
	}
	return suma;
}


///Norma 2
double norma2(double x[], int n){
	double suma=0, res;
	for (int i = 0; i<n; i++){
		suma+=x[i]*x[i];
	}
	res=sqrt(suma);
	return res;
}

///Suma Vectorial
void sumaVectorial(double z[],double x[],double y[],int n){
	for (int i = 0; i<n; i++){
		z[i]=x[i]+y[i];
	}
}

///Resta Vectorial
void restaVectorial(double z[],double x[],double y[],int n){
	for (int i = 0; i<n; i++){
		z[i]=x[i]-y[i];
	}
}

///Swap
void swapear(double &a, double &b){
   	double aux=a;
   	a = b;
   	b = aux;
}


///Factorizacion QR: usando Householder
void factorizacionQR(double *a, double *q, double *r, int n){

	double* u = new double[n];
	double* ei = new double[n];
	double* x = new double[n];
	double* aux = new double[n*n];
	double* qAux = new double[n*n];
	double* rAux = new double[n*n];

	///Inicializar Identidad
	identidad(q,0,n);

	///Inicializar r
	copiarMatriz(r,a,n);

	for(int j = 0; j<n-1; j++){

		cout << "\t\t" << j+1 << " de 783..." << endl;
		///Obtener columna j de a moño (r)
		for(int i = 0; i<j; i++){
			x[i]=0;
		}
		for(int i = j; i<n; i++){
			x[i]=r[i*n+j];
		}

		///Inicializar ei
		for(int i = 0; i<n; i++){
			ei[i]=0;
		}
		ei[j]=norma2(x,n);

		restaVectorial(u,x,ei,n);				///u = x-ei

		///normalizo u
		vectorXEscalar(u,u,1/norma2(u,n),n);		///v = u/-||u||

		 vectorXMatriz(qAux,u,q,n);			///qAux=u'*q
		vectorXVectorTras(aux,u,qAux,n);		///aux = v*v'

		matrizXEscalar(aux,2,n);				///aux = 2*aux

		restaMatricial(q,q,aux,n);			///q' = q-2(*v*(v'*q))

		 vectorXMatriz(rAux,u,r,n);				///qAux=u'*q
		vectorXVectorTras(aux,u,rAux,n);		///aux = v*v'

		matrizXEscalar(aux,2,n);				///aux = 2*aux

		restaMatricial(r,r,aux,n);			///q' = q-2(*v*(v'*r))
	}

	//guardar_matriz(q,"factorizada_q.txt",n,n);
	//guardar_matriz(r,"factorizada_r.txt",n,n);

	trasponerMatriz(q,n);						/// q = transpuesta(qt)

			cout << "Q" << endl;
		imprimirMatriz(q,n);
		cout << "R" << endl;
		imprimirMatriz(r,n);

	delete[] aux;
	delete[] qAux;
	delete[] rAux;
	delete[] u;
	delete[] ei;
	delete[] x;

}



///Vector X Matriz (n elementos, matriz cuadrada)
void vectorXMatriz(double* res,double *z, double *m, int n){
	
	for (int i = 0; i<n; i++){
		res[i]=0;
		for (int j = 0; j<n; j++){
			res[i]=+z[j]*m[i*n+j];
		}
	}
}

///Identidad
void identidad(double *id,int k,int n){
	ceros(id,n,n);

	for (int i = k; i<n; i++){
			id[i*n+i]=1;
		}
}

///Copiar Matriz
void copiarMatriz(double *a, double *b, int n){

    	for (int i = 0; i<n; i++){
        	for (int j = 0; j<n; j++){
            		a[i*n+j]=b[i*n+j];
        	}
    	}

}

///Resta Matricial
void restaMatricial(double* res,double *a,double *b,int n){
	for (int i = 0; i<n; i++){
		for (int j = 0; j<n; j++){
			res[i*n+j]=a[i*n+j]-b[i*n+j];
		}
	}
}


///Inicializar Matriz con 0's
void ceros(double *a,int m,int n){
	for (int i = 0; i<m; i++){
		for (int j = 0; j<n; j++){
			a[i*n+j]=0;
		}
	}
}

///Imprimir matriz
void imprimirMatriz(double *m, int n){
    for (int i = 0; i<n; i++){
        for (int j = 0; j<n; j++){
        cout <<  m[i*n+j] << "  ";
        }
        cout << endl;
    }
}

///Imprimir matriz MN
void imprimirMatrizMN(double *a, int m, int n){
    	for (int i = 0; i<m; i++){
        	for (int j = 0; j<n; j++){
        		cout <<  a[i*n+j] << " ";
        	}
        	cout << endl;
    	}
    	cout << endl;
}

///Guarda vector
void guardarVector(const char* nombre, double *vec, int n){
	
	ofstream salida;
	salida.open(nombre);
	
    for (int i = 0; i<n; i++){
		salida <<  vec[i] << endl;
    }
    
    salida.close();
    
}
void guardarVector(const char* nombre, unsigned long long int *vec, int n){
	
	ofstream salida;
	salida.open(nombre);
	
    for (int i = 0; i<n; i++){
		salida <<  vec[i] << endl;
    }
    
    salida.close();
    
}
void guardarVector(const char* nombre, int *vec, int n){
	
	ofstream salida;
	salida.open(nombre);
	
    for (int i = 0; i<n; i++){
		salida <<  vec[i] << endl;
    }
    
    salida.close();
    
}

///Producto de Matrices de mxn,nxm
void productoMatricialMN(double *axb, double *a, double *b, int m, int n1, int n2, int p){
	double res;
	if (n1==n2){
        for (int i = 0; i<m; i++){
                for (int j = 0; j<p; j++){
                        res=0;
                        for(int k=0; k<n2; k++){
                            res+=a[i*n1+k]*b[k*p+j];
                        }
                        axb[i*p+j] = res;
                }
            }
    	} else {
		cout << "Las dimensiones de las matrices no coinciden." << endl;
	}
}

double productoInterno(double *v, double *w,int n){
	double res = 0;	
	for (int i = 0; i<n; i++){
		res+=v[i]*w[i];
	}
	
	return res;
}

///Imprimir vector
void imprimirVector(double *v,int n){
    	cout <<  "[";
    	for (int i = 0; i<n; i++){
        	cout <<  v[i] << " ";
    	}
    	cout << "]" << endl;
    	cout << endl;
}

int id_nodo(int valor, int* lista, int& n){
	
	int i = 0;
	while(i < n){
		
		if(valor == lista[i]) break;
		i++;
		
	}
	
	if(i == n){
		lista[n] = valor;
		n++;
	}
	
	return (i+1);
	
}

void conversor(const char* archivo_origen, const char* archivo_destino){
	
	ifstream entrada;
	ofstream salida;
	char otronombre[20];
	int* ids;
	int n, m, i = 0;
	int origen, destino;
	
	entrada.open(archivo_origen);
	while(!entrada.good()){
		cout << "El archivo especificado no existe. Ingrese otro:" << endl; 
		cin >> otronombre;
		entrada.open(otronombre);
	}
	salida.open(archivo_destino);
	
	entrada >> n;
	entrada >> m;
	salida << n << endl;
	salida << m << endl;
	
	ids = new int[n];
	
	for(int j = 0; j < m; j++){
		
		entrada >> origen;
		entrada >> destino;
		
		salida << id_nodo(origen,ids,i) << "\t";
		salida << id_nodo(destino,ids,i) << endl;
		
	}
	
	entrada.close();
	salida.close();
	
	delete[] ids;
	
}

void ordenar(const char* nombre, double* autovector, int n){
	
	int* orden = new int[n];
	double aux;
	int aux_i;
	
	ofstream archivo;
	archivo.open(nombre);
	
	for(int i = 0; i < n; i++){
		
		orden[i] = i+1;
		
	}
	
	archivo << "Nodo:\tRank:" << endl;
	
	
	for(int i = 0; i < n-1; i++){
		
		for(int j = i+1; j < n; j++){
			
			if(autovector[i] < autovector[j]){
				aux = autovector[i];
				autovector[i] = autovector[j];
				autovector[j] = aux;
				
				aux_i = orden[i];
				orden[i] = orden[j];
				orden[j] = aux_i;
			}
			
		}
		
		archivo << orden[i] << "\t" << autovector[i] << endl;
		
	}
	
	archivo << orden[n-1] << "\t" << autovector[n-1] << endl;
	
	archivo.close();
	
	delete[] orden;
	
}




