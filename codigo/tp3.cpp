#include <stdio.h>
#include <iostream>
#include <math.h>
#include "esparsa.h"
#include "utils.h"

using namespace std;

double epsilon = 0.00000001; //10^-8
double* v = NULL;
double* vec_aux = NULL;

int calcularGrado(int * matrizCon, const int n,int pagina);
double * calcularP(int *matrizCon, const int n, double * gradosPag);
double * calcularP1(int *matrizCon, const int n);
double * calcularP2(double * P1, const int n, const double c);

void cuadradosMinimos(double * gammas,double * Y,double * yk,int n);
double * extrapolacionQuadratica(double * xm_3,double * xm_2,double * xm,double * xn, int n);
void gramSchmidt(double * Q, double * R, double * A, int n);

int * parser(const char * nombre, int& n);
Esparsa* parser2(const char * nombre, int& n);
Esparsa* parser3(const char * nombre, int &n, int recorte);

void algoritmo_1(Esparsa* P, double*& y, double* x, double c, int n);
double* algoritmo_2(Esparsa* P, double* x0, double c, int iters, int n);
double* algoritmo_6(Esparsa* P, double* x0, double c, int iters, int n,int periodicidad);


void test_1();
void test_2();
void test_3();
void test_tiempos();
void test_dist_a_solucion();
void test_dist_aproximaciones();
void test_c_iteraciones();
void test_c_autovectores();
void test_periodicidad();
void test_validacion();
void test_iteraciones();
double* test_5_algoritmo_6(Esparsa* P, double* x0, double c, int iters, int& n,int periodicidad, double* solucion);
double* test_5_algoritmo_2(Esparsa* P, double* x0, double c, int iters, int& n, double* solucion);
double* test_6_algoritmo_6(Esparsa* P, double* x0, double c, int iters, int& n,int periodicidad);
double* test_6_algoritmo_2(Esparsa* P, double* x0, double c, int iters, int& n);
int test_7_algoritmo_6(Esparsa* P, double* x0, double c, int n,int periodicidad);
int test_7_algoritmo_2(Esparsa* P, double* x0, double c, int n);

int main (){
	
	cout << "--------------PAGERANK--------------" <<  endl;
	 cout <<  endl;
	
	 char entrada[50];
	 
	 char salida[50];
	 
	 double* y;
	 Esparsa* Pt;
	 
	 int n;
	 
	 cout << "Seleccionar opcion: " << endl;
	 
	cout << "1) Calcular autovector PageRank."<< endl;
	cout << "2) Convertir archivo de entrada a un grafo valido."<< endl;
	
	int opcion_conversor = 0;
	cin >> opcion_conversor;
	
	if (opcion_conversor == 1){
		 cout << "Opcion Seleccionada: Calcular autovector PageRank." << endl;
		 double c;
		 
		 cout << "Ingrese nombre del archivo a abrir:" << endl;

		 cin >> entrada;
		 
		 Pt = parser2(entrada,n);
		 
		 cout << "Ingrese nombre del archivo de salida a crear con los PageRanks:" << endl;
		 
		 cin >> salida;
		 
		 cout << "Ingrese valor de probabilidad de teletransportación (c):" << endl;
		 
		 cin >> c;
		 
		 while (c<=0 || c>=1) {
				 
			 cout << "El c ingresado no se encuentra en el intervalo (0,1), ingresar nuevamente:" << endl;
			 
			 cin >> c;
		 }
		 
		 Pt->a_semi_estocastica();
		 
		 int opcionMetodo;
		 int periodicidad;
		 
		 cout << "Ingrese Metodo para calcular el autovector PageRank:" << endl;
		 cout << "1) Metodo de la Potencia."<< endl;
		 cout << "2) Modificacion con Extrapolacion Cuadratica."<< endl;
		 
		 cin >> opcionMetodo;
		 
		 cout << "Ingrese la cantidad de iteraciones a realizar:"<< endl;
		 int iteraciones;
		 cin >> iteraciones;
		 
		 if (opcionMetodo==1) {
			 y = algoritmo_2(Pt,v,c,iteraciones,n);
		 } else if (opcionMetodo==2){
			 cout << "Ingrese la periodicidad con la que se llamara a QE: " << endl;	
			 cin >> periodicidad;
			 y = algoritmo_6(Pt,v,c,iteraciones,n,periodicidad);
		 } else {
			 
			 cout << "Opcion incorrecta."<< endl;
			delete[] v;
			delete[] vec_aux;
			delete Pt;
			 
		}
		 if (opcionMetodo==1 || opcionMetodo==2){
					
			 ordenar(salida,y,n);
			 
			 cout << "Salida almacenado en el archivo: " << salida << endl;
	 		 delete[] v;
			 delete[] vec_aux;
			 delete Pt;
			 delete[] y;
		 
		 }

	}
	else if (opcion_conversor==2) {	
		cout << "Opcion Seleccionada: Convertir archivo de entrada a un grafo valido."<< endl;
		char entrada_conversor[20];
	 
		char salida_conversor[20];
	     cout << "Ingrese nombre del archivo a convertir:" << endl;

		 cin >> entrada_conversor;
		 
		 cout << "Ingrese nombre del archivo de salida del conversor:" << endl;

		 cin >> salida_conversor;
		 
	     conversor(entrada_conversor,salida_conversor);
	     
	     cout << "Grafo convertido almacenado en el archivo: " << salida_conversor << endl;
	} else  cout << "Opcion incorrecta." << endl;
	
	//~ test_1();
	
	return 0;
}

///Comprueba empiricamente lo pedido en el punto 1 del enunciado
void test_1(){
	
	const char* entrada = "graph.out";
	const char* salida1 = "tiempo_esparsa.out";
	const char* salida2 = "tiempo_comun.out";
	
	unsigned long long int tiempos_esparsa[3];
	unsigned long long int tiempos_comun[3];
	unsigned long long int _start_, _end_, total1, total2;
	
	double c = 0.5;
	double* y;
	double* P2;
	double* P1;
	Esparsa* Pt;
	int* matrizCon;
	int n;
	
	double* v_a;
	double* v_b;
	double* v_c;
	double espaciado;
	const double tercio = 3./10.;
	
	matrizCon = parser(entrada,n);
	Pt = parser2(entrada,n);
	
	y = new double[n];
	v_a = new double[n];
	v_b = new double[n];
	v_c = new double[n];
	
	espaciado = 1./((double)(n/2));
	
	for(int i = 0; i < n; i++){
		
		v_a[i] = 0.0;
		v_b[i] = 0.0;
		if(fmod(i,2)) v_c[i] = espaciado;
		else v_c[i] = 0.0;
		
	}
	
	v_a[0] = 1.;
	v_b[0] = tercio;
	v_b[n-1] = tercio;
	v_b[n/2] = tercio;
	
	P1 = calcularP1(matrizCon,n);
	P2 = calcularP2(P1,n,c);
	
	Pt->a_semi_estocastica();
	
	MEDIR_TIEMPO_START(_start_);
	algoritmo_1(Pt,y,v_a,c,n);
	MEDIR_TIEMPO_STOP(_end_);
	total2 = _end_ - _start_;
	tiempos_esparsa[0] = total2;
	guardarVector("vec_esparsa_a.out",y,n);
	
	MEDIR_TIEMPO_START(_start_);
	matrizXVector(P2,v_a,y,n);
	MEDIR_TIEMPO_STOP(_end_);
	total1 = _end_ - _start_;
	tiempos_comun[0] = total1;
	guardarVector("vec_comun_a.out",y,n);
	
	MEDIR_TIEMPO_START(_start_);
	algoritmo_1(Pt,y,v_b,c,n);
	MEDIR_TIEMPO_STOP(_end_);
	total2 = _end_ - _start_;
	tiempos_esparsa[1] = total2;
	guardarVector("vec_esparsa_b.out",y,n);
	
	MEDIR_TIEMPO_START(_start_);
	matrizXVector(P2,v_b,y,n);
	MEDIR_TIEMPO_STOP(_end_);
	total1 = _end_ - _start_;
	tiempos_comun[1] = total1;
	guardarVector("vec_comun_b.out",y,n);
	
	MEDIR_TIEMPO_START(_start_);
	algoritmo_1(Pt,y,v_c,c,n);
	MEDIR_TIEMPO_STOP(_end_);
	total2 = _end_ - _start_;
	tiempos_esparsa[2] = total2;
	guardarVector("vec_esparsa_c.out",y,n);
	
	MEDIR_TIEMPO_START(_start_);
	matrizXVector(P2,v_c,y,n);
	MEDIR_TIEMPO_STOP(_end_);
	total1 = _end_ - _start_;
	tiempos_comun[2] = total1;
	guardarVector("vec_comun_c.out",y,n);

	guardarVector(salida1,tiempos_comun,3);
	guardarVector(salida2,tiempos_esparsa,3);

	delete[] v_a;
	delete[] v_b;
	delete[] v_c;
	delete[] y;
	delete Pt;
	delete[] P1;
	delete[] P2;
	delete[] v;
	delete[] vec_aux;
	delete[] matrizCon;
	
}

///Guarda los autovectores devueltos por los algoritmos 2 y 6 para comprobar su similitud.
void test_2(){
	
	const char* entrada = "graph.out";
	const char* salida1 = "autovec_algo_2.out";
	const char* salida2 = "autovec_algo_6.out";
	
	double* y;
	Esparsa* Pt;
	
	int n;
	double c = 0.5;
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	y = algoritmo_2(Pt,v,c,20,n);
	guardarVector(salida1,y,n);

	y = algoritmo_6(Pt,v,c,20,n,27);
	guardarVector(salida2,y,n);

	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
	
}

///Revela las 5 paginas con mas relevancia (indica el nodo).
void test_3(){
	
	const char* entrada = "graph_example.in";
	const char* salida1 = "autovec_algo_2.out";
	
	int aux;
	double* y;
	Esparsa* Pt;
	
	int n;
	double c = 0.5;
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	y = algoritmo_2(Pt,v,c,20,n);
	guardarVector(salida1,y,n);

	cout << "Los 5 nodos mas relevantes:" << endl;
	cout << "(del mas importante al menos importante)" << endl << endl;


	for(int i = 0;i < 5; i++){
		
		aux = 0;
		
		for(int j = 1;j < n; j++){
			
			if(y[aux] < y[j]){
				
				aux = j;
				
			}
			
		}
		y[aux] = 0.0;
		cout << "\tNodo " << aux+1 << endl;
		
	}

	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
	
}

///Mediciones de tiempo sobre los algoritmos 2 y 6.
void test_tiempos(){
	
	const char* entrada = "wiki.out";
	const char* salida1 = "tiempo_algo_2.out";
	const char* salida2 = "tiempo_algo_6.out";
	const char* salida3 = "cant_nodos.out";
	ofstream archivo1, archivo2;
	
	double* y;
	Esparsa* Pt;
	int n;
	int tam_recorte;
	double c = 0.5;
	int iter = 1000;
	int periodicidad = 10;
	int rango_recorte = 1500;
	
	unsigned long long int* tiempos_algo_2 = new unsigned long long int[rango_recorte];
	unsigned long long int* tiempos_algo_6 = new unsigned long long int[rango_recorte];
	unsigned long long int _start_, _end_, total1, total2;
	int* cant_nodos = new int[rango_recorte];
	v = new double[7115];
	vec_aux = new double[7115];
	
	for(int j = 1;j <= rango_recorte; j++){
	
		cout << "Iteracion " << j << " de " << rango_recorte << endl;
		
		cout << "\tConstruyendo matriz..." << endl;
		tam_recorte = j * 4;
	
		Pt = parser3(entrada,n,tam_recorte);
	
		Pt->a_semi_estocastica();
	
		total1 = 0;
		total2 = 0;
	
		cout << "\tn: " << n << endl;
		cout << "\tMidiendo tiempos..." << endl;
		
		MEDIR_TIEMPO_START(_start_);
		y = algoritmo_6(Pt,v,c,iter,n,periodicidad);
		MEDIR_TIEMPO_STOP(_end_);
		total2 += _end_ - _start_;
		
		delete[] y;
		
		MEDIR_TIEMPO_START(_start_);
		y = algoritmo_2(Pt,v,c,iter,n);
		MEDIR_TIEMPO_STOP(_end_);
		total1 += _end_ - _start_;
		
		delete[] y;

		cout << "\tFin Midiendo tiempos." << endl << endl;
		
		tiempos_algo_2[rango_recorte-j] = total1 /*/ 10*/;
		tiempos_algo_6[rango_recorte-j] = total2 /*/ 10*/;
		cant_nodos[rango_recorte-j] = n;
		
		
		delete Pt;
		
	}
	
	guardarVector(salida1,tiempos_algo_2,rango_recorte);
	guardarVector(salida2,tiempos_algo_6,rango_recorte);
	guardarVector(salida3,cant_nodos,rango_recorte);

	delete[] v;
	delete[] vec_aux;
	delete[] tiempos_algo_2;
	delete[] tiempos_algo_6;
	delete[] cant_nodos;
	
}

///Evalua la distancia de las sucesivas aproximaciones del algoritmo 2 y 6 a una solucion.
void test_dist_a_solucion(){
	
	const char* entrada = "Salida33.txt";
	const char* salida1 = "dif_algo_2.out";
	const char* salida2 = "dif_algo_6.out";
	
	double* y;
	Esparsa* Pt;
	double* difs;
	
	int n;
	int n_copia;
	double c = 0.95;
	int periodicidad = 2;
	int iters = 1000;
	
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	y = algoritmo_2(Pt,v,c,iters,n);

	n_copia = n;
	difs = test_5_algoritmo_2(Pt,v,c,iters,n_copia,y);
	guardarVector(salida1,difs,n_copia);

	delete[] difs;

	n_copia = n;
	difs = test_5_algoritmo_6(Pt,v,c,iters,n_copia,periodicidad,y);
	guardarVector(salida2,difs,n_copia);

	delete[] difs;
	
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
	
}

///Evalua la distancia entre las sucesivas aproximaciones del algoritmo 2 y 6.
void test_dist_aproximaciones(){
	
	const char* entrada = "Salida33.txt";
	const char* salida1 = "dist_algo_2.out";
	const char* salida2 = "dist_algo_6.out";
	
	double* y;
	Esparsa* Pt;
	double* difs;
	
	int n;
	int n_copia;
	double c = 0.95;
	int periodicidad = 2;
	int iters = 1000;
	
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	y = algoritmo_2(Pt,v,c,iters,n);

	n_copia = n;
	difs = test_6_algoritmo_2(Pt,v,c,iters,n_copia);
	guardarVector(salida1,difs,n_copia);

	delete[] difs;

	n_copia = n;
	difs = test_6_algoritmo_6(Pt,v,c,iters,n_copia,periodicidad);
	guardarVector(salida2,difs,n_copia);

	delete[] difs;
	
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
	
}

///Variando c: cuento iteraciones.
void test_c_iteraciones(){
	
	const char* entrada = "Salida33.txt";
	const char* salida1 = "it_algo_2.out";
	const char* salida2 = "it_algo_6.out";
	const char* salida3 = "cs.out";
	
	Esparsa* Pt;	
	
	int n;
	int salto;
	double c;
	int periodicidad = 2;
	//~ int iters = 1000;
	int cant_muestras = 19;
	double* cs = new double[cant_muestras];
	int* it_algo_2 = new int[cant_muestras];
	int* it_algo_6 = new int[cant_muestras];
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	salto = 100 / (cant_muestras+1);
	
	for(int i = 0; i < cant_muestras; i++){
	
		cout << "Evaluando con Muestra " << i+1 << " de " << cant_muestras << "." << endl;
	
		c = ((double)((i+1)*salto))*0.01;
	
		it_algo_6[i] = test_7_algoritmo_6(Pt,v,c,n,periodicidad);
		it_algo_2[i] = test_7_algoritmo_2(Pt,v,c,n);
		cs[i] = c;

	}
	
	guardarVector(salida1,it_algo_2,cant_muestras);
	guardarVector(salida2,it_algo_6,cant_muestras);
	guardarVector(salida3,cs,cant_muestras);

	delete[] it_algo_2;
	delete[] it_algo_6;
	delete[] cs;
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	
}

///Variando c: Calidad del Autovector
void test_c_autovectores(){
	
	const char* entrada = "Salida33.txt";
	const char* salida1 = "AV_algo_2.out";
	const char* salida2 = "AV_algo_6.out";
	const char* salida3 = "csAV.out";
	
	double* y;
	Esparsa* Pt;	
	
	int n;
	int salto;
	double c;
	int periodicidad = 2;
	int iters = 1000;
	int cant_muestras = 19;
	double* cs = new double[cant_muestras];
	double* AV_algo_2 = new double[cant_muestras];
	double* AV_algo_6 = new double[cant_muestras];
	Pt = parser2(entrada,n);
	double* aux = new double[n];
	Pt->a_semi_estocastica();
	
	salto = 100 / (cant_muestras+1);
	
	for(int i = 0; i < cant_muestras; i++){
	
		cout << "Evaluando con Muestra " << i+1 << " de " << cant_muestras << "." << endl;
	
		c = ((double)((i+1)*salto))*0.01;
	
		//ALGORITMO 6
		y = algoritmo_6(Pt,v,c,iters,n,periodicidad);
		algoritmo_1(Pt,aux,y,c,n);
	
		cout << "	PxX" << endl;
		//imprimirVector(aux,n);
	
		cout << "	X" << endl;
		//imprimirVector(y,n);
		restaVectorial(aux,aux,y,n);
	
		AV_algo_6[i] = norma2(aux,n);
		
		//ALGORITMO 2
		y = algoritmo_2(Pt,v,c,iters,n);
		algoritmo_1(Pt,aux,y,c,n);
	
		cout << "	PxX" << endl;
		//imprimirVector(aux,n);
	
		cout << "	X" << endl;
		//imprimirVector(y,n);
		restaVectorial(aux,aux,y,n);
		
		AV_algo_2[i] = norma2(aux,n);
		cs[i] = c;

	}
	
	guardarVector(salida1,AV_algo_2,cant_muestras);
	guardarVector(salida2,AV_algo_6,cant_muestras);
	guardarVector(salida3,cs,cant_muestras);

	delete[] aux;
	delete[] AV_algo_2;
	delete[] AV_algo_6;
	delete[] cs;
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	
}

void test_iteraciones(){
	const char* entrada = "graph.out";
	
	const char* salida = "test_iteraciones.out";
	
	double c=0.95;
	
	int iteraciones = 1000;
	
	double* y;
	Esparsa* Pt;
	
	int n;
	int periodicidad = 10;
	
	cout << "Test Iteraciones:" << endl;

	cout << "Archivo de entrada: " << entrada << endl;
	
	Pt = parser2(entrada,n);
	
	cout << "Archivo de salida: " << salida << endl;
	
	Pt->a_semi_estocastica();
	
	cout << "Calculando con Metodo de la Potencia."<< endl;
	
	y = algoritmo_2(Pt,v,c,iteraciones,n);
	
	cout << "Calculando con QE."<< endl;
	
	y = algoritmo_6(Pt,v,c,iteraciones,n,periodicidad);
	
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;	
}
void test_periodicidad(){

	///TEST QUE VARIA PERIODICIDAD
	
	const char* entrada = "wiki.out";
	
	const char* salida = "test_per2.out";
	
	int iteraciones = 50; //<--------------- Consultar por que esto afecta demasiado este test!!!
	
	double* y;
	Esparsa* Pt;
	
	int n;
	int periodicidad = 30; //<-------------- a partir de 7 llamados se estabiliza masomenos
	
	double* difs = new double [periodicidad];
	
	double c=0.95;
	
	cout << "Test Periodicidad:" << endl;

	cout << "Archivo de entrada: " << entrada << endl;
	
	Pt = parser2(entrada,n);
	
	cout << "Archivo de salida: " << salida << endl;
	
	Pt->a_semi_estocastica();
	

	cout << "Calculando con Metodo de la Potencia."<< endl;
	
	double* qes;
	
	y = algoritmo_2(Pt,v,c,iteraciones,n);
	cout << "Y:"<< endl;
	imprimirVector(y,n);
	cout << "Calculando con QE. Variando llamados entre 1 y "<< periodicidad << endl;
	
	for (int i = 0; i < periodicidad; i++){
		qes = algoritmo_6(Pt,v,c,iteraciones,n,i+1);

		restaVectorial(vec_aux,qes,y,n);
		difs[i]=norma1(vec_aux,n);
	}
	
	guardarVector(salida,difs,periodicidad);
	
	delete[] difs;
	delete[] qes;
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
}

void test_validacion(){
		
	const char* entrada = "wiki.out";
	//const char* entrada2 = "graph_example.in";
	//const char* salida1 = "test_per.out";
	
	int iteraciones = 20; //<--------------- Consultar por que esto afecta demasiado este test!!!
	
	double* y;
	Esparsa* Pt;
	
	int n;
	int periodicidad = 1; //<-------------- a partir de 7 llamados se estabiliza masomenos
	
	
	double c=0.95;
	
	cout << "Test Validacion:" << endl;

	cout << "Archivo de entrada: " << entrada << endl;
	
	Pt = parser2(entrada,n);
	
	Pt->a_semi_estocastica();
	
	cout << "	Algoritmo2:" << endl;
	
	y = algoritmo_2(Pt,v,c,iteraciones,n);
	
	if (norma1(y,n)-1<epsilon || norma1(y,n)==1) cout << "		Norma 1: Ok" << endl;
		else 			cout << "		Norma 1: DISTINTO DE 1 => " << norma1(y,n) << endl;
	
	////test---------------------------------------
	double* aux = new double[n];
	
	algoritmo_1(Pt,aux,y,c,n);
	
	cout << "	PxX" << endl;
	//imprimirVector(aux,n);
	
	cout << "	X" << endl;
	//imprimirVector(y,n);
	
	restaVectorial(aux,aux,y,n);
	
	cout << "	Error Absoluto: " << norma2(aux,n)<< endl;
	
	if (norma2(aux,n)<epsilon) cout << "		Es Autovector?: SI" << endl;
	else 			cout << "		Es Autovector?: DIFERENCIA MAS GRANDE QUE EPSILON =" << epsilon << endl;
	
	////test---------------------------------------
	
	cout << endl;
	cout << "	Algoritmo6:" << endl;
	
	y = algoritmo_6(Pt,v,c,iteraciones,n,periodicidad);
	
	if (norma1(y,n)-1<epsilon || norma1(y,n)==1) cout << "		Norma 1: Ok" << endl;
		else 			cout << "		Norma 1: DISTINTO DE 1 => Era igual a:" << norma1(y,n) << endl;
	
	////test---------------------------------------
	double* aux2 = new double[n];
	
	algoritmo_1(Pt,aux,y,c,n);
	
	cout << "	PxX" << endl;
	//imprimirVector(aux,n);
	
	cout << "	X" << endl;
	//imprimirVector(y,n);
	
	restaVectorial(aux,aux,y,n);
	
	cout << "	Error Absoluto: " << norma2(aux,n)<< endl;
	
	if (norma2(aux,n)<epsilon) cout << "		Es Autovector?: SI" << endl;
	else 			cout << "		Es Autovector?: DIFERENCIA MAS GRANDE QUE EPSILON =" << epsilon << endl;
	delete[] aux;
	delete[] aux2;
	////test---------------------------------------
	delete[] v;
	delete[] vec_aux;
	delete Pt;
	delete[] y;
}

///Kamvar et al: Algoritmo 1
void algoritmo_1(Esparsa* P, double*& y, double* x, double c, int n){
	
	double w = norma1(x,n);
	
	P->mul_vector(y,x,n);
	vectorXEscalar(y,y,c,n);
	
	w -= norma1(y,n);
	
	vectorXEscalar(vec_aux,v,w,n);
	
	sumaVectorial(y,y,vec_aux,n);
	
}

///Kamvar et al: Algoritmo 2
double* algoritmo_2(Esparsa* P, double* x0, double c, int iters, int n){
	
	int i = 0;
	double* xn = new double[n];
	double* xm = new double[n];
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm,xn,c,n);
		algoritmo_1(P,xn,xm,c,n);
		
		restaVectorial(vec_aux,xn,xm,n);
		
		i+= 2;
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);
	
	///Normalizar el vector xn
	vectorXEscalar(xn,xn,1/norma1(xn,n),n);	
	
	delete[] xm;
	return xn;
	
}

///Modificasiones sobre el algoritmo_2 para poder evaluarlo internamente en el test_5
double* test_5_algoritmo_2(Esparsa* P, double* x0, double c, int iters, int& n, double* solucion){
	
	int i = 0;
	double* xn = new double[n];
	double* xm = new double[n];
	double* distancias = new double[iters+5];
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm,xn,c,n);
		restaVectorial(vec_aux,solucion,xm,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
		algoritmo_1(P,xn,xm,c,n);
		restaVectorial(vec_aux,solucion,xn,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
		
		restaVectorial(vec_aux,xn,xm,n);
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);
	
	cout << "Terminado algoritmo_2 a las " << i << " iteraciones." << endl;
	
	delete[] xm;
	delete[] xn;
	
	n = i;
	return distancias;
	
}

///Modificasiones sobre el algoritmo_2 para poder evaluarlo internamente en el test_5
double* test_6_algoritmo_2(Esparsa* P, double* x0, double c, int iters, int& n){
	
	int i = 0;
	double* xn = new double[n];
	double* xm = new double[n];
	double* distancias = new double[iters+5];
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm,xn,c,n);
		restaVectorial(vec_aux,xn,xm,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
		
		algoritmo_1(P,xn,xm,c,n);
		restaVectorial(vec_aux,xm,xn,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);
	
	cout << "Terminado algoritmo_2 a las " << i << " iteraciones." << endl;
	
	delete[] xm;
	delete[] xn;
	
	n = i;
	return distancias;
	
}

///Modificasiones sobre el algoritmo_2 para poder contar iteraciones
int test_7_algoritmo_2(Esparsa* P, double* x0, double c, int n){
	
	int i = 0;
	double* xn = new double[n];
	double* xm = new double[n];
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm,xn,c,n);
		i++;
		
		algoritmo_1(P,xn,xm,c,n);
		i++;
		
		restaVectorial(vec_aux,xm,xn,n);
		
	}while(norma1(vec_aux,n)>epsilon);
	
	delete[] xm;
	delete[] xn;
	
	return i;
	
}

///Kamvar et al: Algoritmo 6
double* algoritmo_6(Esparsa* P, double* x0, double c, int iters, int n,int periodicidad){

	double* xn = new double[n];
	double* xm = new double[n];
	double* xm_2 = new double[n];
	double* xm_3 = new double[n];
	
	int i = 0;
	int j = 0;
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm_3,xn,c,n);
	
		algoritmo_1(P,xm_2,xm_3,c,n);
	
		algoritmo_1(P,xm,xm_2,c,n);
	
		algoritmo_1(P,xn,xm,c,n);
		
		j++;
		
		///Periodicamente
		if (fmod(j,periodicidad)==0)
		 xn=extrapolacionQuadratica(xm_3,xm_2,xm,xn,n);
		
		restaVectorial(vec_aux,xn,xm,n);

		i+= 4;
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);
	///Normalizar el vector xn
	vectorXEscalar(xn,xn,1/norma1(xn,n),n);

	delete[] xm;
	delete[] xm_2;
	delete[] xm_3;
	return xn;
}

///Modificasiones sobre el algoritmo_6 para poder evaluarlo internamente en el test_5
double* test_5_algoritmo_6(Esparsa* P, double* x0, double c, int iters, int& n,int periodicidad, double* solucion){

	double* xn = new double[n];
	double* xm = new double[n];
	double* xm_2 = new double[n];
	double* xm_3 = new double[n];
	double* distancias = new double[iters+5];
	
	int i = 0;
	int j = 0;
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm_3,xn,c,n);
		restaVectorial(vec_aux,solucion,xm_3,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xm_2,xm_3,c,n);
		restaVectorial(vec_aux,solucion,xm_2,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xm,xm_2,c,n);
		restaVectorial(vec_aux,solucion,xm,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xn,xm,c,n);
		restaVectorial(vec_aux,solucion,xn,n);
		distancias[i] = norma2(vec_aux,n);
		
		j++;
		
		///Periodicamente
		if (fmod(j,periodicidad)==0){
			xn=extrapolacionQuadratica(xm_3,xm_2,xm,xn,n);
			restaVectorial(vec_aux,solucion,xn,n);
			distancias[i] = norma2(vec_aux,n);
			cout << "Iteracion: " << i+1 << endl;
		}
		i++;
		
		restaVectorial(vec_aux,xn,xm,n);
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);

	cout << "Terminado algoritmo_6 a las " << i << " iteraciones." << endl;

	delete[] xn;
	delete[] xm;
	delete[] xm_2;
	delete[] xm_3;
	
	n = i;
	return distancias;
}

double* test_6_algoritmo_6(Esparsa* P, double* x0, double c, int iters, int& n,int periodicidad){

	double* xn = new double[n];
	double* xm = new double[n];
	double* xm_2 = new double[n];
	double* xm_3 = new double[n];
	double* distancias = new double[iters+5];
	
	int i = 0;
	int j = 0;
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm_3,xn,c,n);
		restaVectorial(vec_aux,xn,xm_3,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xm_2,xm_3,c,n);
		restaVectorial(vec_aux,xm_3,xm_2,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xm,xm_2,c,n);
		restaVectorial(vec_aux,xm_2,xm,n);
		distancias[i] = norma2(vec_aux,n);
		i++;
	
		algoritmo_1(P,xn,xm,c,n);
		restaVectorial(vec_aux,xm,xn,n);
		distancias[i] = norma2(vec_aux,n);
		
		j++;
		
		///Periodicamente
		if (fmod(j,periodicidad)==0){
			xn=extrapolacionQuadratica(xm_3,xm_2,xm,xn,n);
			restaVectorial(vec_aux,xm,xn,n);
			distancias[i] = norma2(vec_aux,n);
			cout << "EQ en la Iteracion: " << i+1 << endl;
		}
		
		i++;
		
		restaVectorial(vec_aux,xn,xm,n);
		
	}while(i<iters && norma1(vec_aux,n)>epsilon);

	cout << "Terminado algoritmo_6 a las " << i << " iteraciones." << endl;

	delete[] xn;
	delete[] xm;
	delete[] xm_2;
	delete[] xm_3;
	
	n = i;
	return distancias;
}

///Modificasiones sobre el algoritmo_6 para poder contar iteraciones
int test_7_algoritmo_6(Esparsa* P, double* x0, double c, int n,int periodicidad){

	double* xn = new double[n];
	double* xm = new double[n];
	double* xm_2 = new double[n];
	double* xm_3 = new double[n];
	
	int i = 0;
	int j = 0;
	
	copy(x0,x0+n,xn);
	
	do{
		
		algoritmo_1(P,xm_3,xn,c,n);
		i++;
	
		algoritmo_1(P,xm_2,xm_3,c,n);
		i++;
	
		algoritmo_1(P,xm,xm_2,c,n);
		i++;
	
		algoritmo_1(P,xn,xm,c,n);
		
		j++;
		
		///Periodicamente
		if (fmod(j,periodicidad)==0){
			xn=extrapolacionQuadratica(xm_3,xm_2,xm,xn,n);
		}
		
		i++;
		
		restaVectorial(vec_aux,xn,xm,n);
		
	}while(norma1(vec_aux,n)>epsilon);

	delete[] xn;
	delete[] xm;
	delete[] xm_2;
	delete[] xm_3;
	
	return i;
}

///Calcula Extrapolacion Cuadratica
double * extrapolacionQuadratica(double * xm_3,double * xm_2,double * xm,double * xn, int n){
	
	///Inicializar yk's
	
	double* yk = new double[n];
	double* yk_1 = new double[n];
	double* yk_2 = new double[n];
	double * beta0xm_2 = new double[n];
	double * beta1xm = new double[n];
	double * beta2xn = new double[n];
	
	restaVectorial(yk_2,xm_2,xm_3,n);
	restaVectorial(yk_1,xm,xm_3,n);
	restaVectorial(yk,xn,xm_3,n); 
	
	///Inicializar Y
	
	double* Y = new double[2*n]; // Y tiene n filas y 2 columnas
	
	for (int i=0; i < n;i++) Y[i*2]=yk_2[i];
	for (int i=0; i < n;i++) Y[i*2+1]=yk_1[i];		
	
	///Vector Gamma y Beta
	
	double* gammas = new double[4];
	double* betas = new double[3];
	
	gammas[3] = 1;
	
	cuadradosMinimos(gammas,Y,yk,n);
	
	gammas[0]=-gammas[1]-gammas[2]-gammas[3];
	
	betas[0]=gammas[1]+gammas[2]+gammas[3];
	betas[1]=gammas[2]+gammas[3];
	betas[2]=gammas[3];
	
	vectorXEscalar(beta0xm_2,xm_2,betas[0],n);
	vectorXEscalar(beta1xm,xm,betas[1],n);
	vectorXEscalar(beta2xn,xn,betas[2],n);
	
	sumaVectorial(xn,beta0xm_2,beta1xm,n); 
	sumaVectorial(xn,xn,beta2xn,n);
	
	///Normalizar el vector xn
	vectorXEscalar(xn,xn,1/norma1(xn,n),n);
	
	delete[] yk;
	delete[] yk_1;
	delete[] yk_2;
	delete[] Y;
	delete[] gammas;
	delete[] betas;
	delete[] beta0xm_2;
	delete[] beta1xm;
	delete[] beta2xn;
	
	return xn;
}



void gramSchmidt(double * Q, double * R, double * A, int n){
	
	/**
	* Esta funcion esta implementada 
	* solamente para el caso donde 
	* A tiene n filas y 2 columnas
	*/
	
	///Inicializar Q y R

	ceros(Q,n,2);
	ceros(R,2,2);
	
	double * vec = new double [n];
	double * aux = new double  [n];
	double * Qaux = new double [n];
	
	///Obtener primera columna de A
	for (int i=0; i < n; i++){
		vec[i]=A[i*2];
	}
	
	R[0]=norma2(vec,n);
	copy(vec,vec+n,aux); ///aux=u1=v1
	vectorXEscalar(Qaux,vec,1/R[0],n);
	
	///Construir primera columna de Q
	for (int i=0; i < n; i++){
		Q[i*2]=Qaux[i];
	} 
	
	///Obtener segunda columna de A
	for (int i=0; i < n; i++){
		vec[i]=A[i*2+1];
	}
	
	R[1]=productoInterno(aux,vec,n);
	vectorXEscalar(aux,aux,pow(R[0],2),n);
	restaVectorial(vec,vec,aux,n);
	
	R[3]=norma2(vec,n);
	
	vectorXEscalar(aux,vec,1/R[3],n);
	///Construir segunda columna de Q
	for (int i=0; i < n; i++){
		Q[i*2+1]=aux[i];
	}	
	
	delete [] vec;
	delete [] aux;
	delete [] Qaux;
	
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P1 (P' Kamvar et al).
double * calcularP1(int * matrizCon,const int n){
    double * gradosPag = new double [n];
    
    for (int i=0; i < n; i++){
        gradosPag[i]=calcularGrado(matrizCon,n,i);
    }
    
    double * P1 = calcularP(matrizCon,n, gradosPag);
    
    double * d = new double[n];
    
    for(int i=0; i < n; i++){
        if (gradosPag[i]==0) d[i]=1;
        else d[i]=0;
    }
    
    double * D = new double[n*n];
    vectorXVectorTras(D,v,d,n);
    
    sumaMatricial(P1,P1,D,n);
    
    delete[] gradosPag;
    delete[] d;
    delete[] D;
    
    return P1;
}

///Calcula Cuadrados Minimos
void cuadradosMinimos(double * gammas,double * Y,double * yk,int n){
	double * Q = new double [2*n];
	double * R = new double [4];
	
	gramSchmidt(Q,R,Y,n); ///Obtener fact QR de Y
	
	matrizXEscalarMN(Q, -1, n,2); ///Q=-Q
	
	trasponerMatrizMN(Q,n,2); ///Q=Q'
	
	double Qty[2] ; ///obtengo -Q'*y
	productoMatricialMN(Qty, Q, yk, 2, n, n, 1);
	
	///backward para resolver R*(gammas[1] gammas[2])'=_Qty
	gammas[2]=Qty[1]/R[3];
	gammas[1]=(Qty[0]-R[1]*gammas[2])/R[0];
	
	delete [] Q;
	delete [] R;
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P2 (P'' Kamvar et al).
double * calcularP2(double * P1,const int n, const double c){
    double * unos = new double[n];
    
        for(int i=0; i < n; i++){
           unos[i]=1;
        }

    double * E = new double[n*n];
    double * P2 = new double[n*n];
    
    vectorXVectorTras(E,v,unos,n);
    
    matrizXEscalar(P1, c, n);
    matrizXEscalar(E, 1-c, n);
    
    sumaMatricial(P2,P1,E,n);
    
    delete[] unos;
    delete[] E;
    
    return P2;
    
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula P (P Kamvar et al).
double * calcularP(int * matrizCon,const int n, double * gradosPag){
    double * P = new double [n*n];
    
    for(int i=0;i < n; i++)
        for(int j=0;j < n; j++){
            if (matrizCon[i*n+j]!=0) P[i*n+j] = 1/gradosPag[j];
            else P[i*n+j]=0;
        }
    return P;
}

///Usado para test 1: implementacion sin optimizaciones.
///Calcula el grado de cada nodo.
int calcularGrado(int * matrizCon,const int n,int pagina){
    int grado = 0;
    for (int i=0; i < n; i++){
        grado += matrizCon[i*n+pagina];
    }
    return grado;
}

///Para test 1
int * parser(const char * nombre, int &n){
	
	ifstream archivo;
	char otronombre[20];
	int m;
	int origen, destino;
	int * matrizCon;
	
	archivo.open(nombre);
	while(!archivo.good()){
		cout << "El archivo especificado no existe. Ingrese otro:" << endl; 
		cin >> otronombre;
		archivo.open(otronombre);
	}
	
	archivo >> n;
	archivo >> m;
	
    matrizCon = new int [n*n];
    
    if(v == NULL){
		
		v = new double[n];
		vec_aux = new double[n];
		
		for(int i=0; i < n; i++){
		
			v[i]=1.0/(double)n;
           
		}
	
	}
	
	//Inicializar la matriz
	for(int i = 0; i < n; i++)
	    for(int j = 0; j < n; j++){
	        matrizCon[i*n+j]=0;
	    }
	
	for(int i = 0; i < m; i++){
		
		archivo >> origen;
		archivo >> destino;
		destino=destino-1;
		origen=origen-1;
		matrizCon[destino*n+origen]=1;
		
	}
	
	archivo.close();
	return matrizCon;
}


Esparsa* parser2(const char * nombre, int &n){
	
	ifstream archivo;
	char otronombre[20];
	int m;
	Esparsa* P;
	int origen, destino;
	
	archivo.open(nombre);
	while(!archivo.good()){
		cout << "El archivo especificado no existe. Ingrese otro:" << endl; 
		cin >> otronombre;
		archivo.open(otronombre);
	}
	
	archivo >> n;
	archivo >> m;
	
	if(v == NULL){
		
		v = new double[n];
		vec_aux = new double[n];
		
		for(int i=0; i < n; i++){
		
			v[i]=1.0/(double)n;
           
		}
	
	}
	
	if(m <= (long long)n*(n-1)){
	
		P = new Esparsa(n);
	
		for(int i = 0; i < m; i++){
		
			archivo >> origen;
			archivo >> destino;
			P->relacion(origen,destino);
		
		}
	
		archivo.close();
		return P;
		
	}else{
		
		cout << "ERROR: Aristas de mas." << endl;
		return NULL;
		
	}
	
}

///Para test 4
Esparsa* parser3(const char * nombre, int &n, int recorte){
	
	ifstream archivo;
	char otronombre[20];
	int m;
	Esparsa* P;
	int origen, destino;
	
	archivo.open(nombre);
	while(!archivo.good()){
		cout << "El archivo especificado no existe. Ingrese otro:" << endl; 
		cin >> otronombre;
		archivo.open(otronombre);
	}
	
	archivo >> n;
	archivo >> m;
	
	n -= recorte;
	
	for(int i=0; i < n; i++){
	
		v[i]=1.0/(double)n;
          
	}
	
	P = new Esparsa(n);

	for(int i = 0; i < m; i++){
	
		archivo >> origen;
		archivo >> destino;
		if(origen <= n && destino <= n) P->relacion(origen,destino);
	
	}

	archivo.close();
	return P;
	
}
