#include "esparsa.h"

Esparsa::Esparsa(int n){
	
	tam = n;
	
	matriz = new ESPARSA[tam];
	
	intermedio = new double[tam];
	
}

Esparsa::~Esparsa(){
	
	delete[] matriz;
	delete[] intermedio;
	
}

void Esparsa::relacion(int i, int j){
	
	i = i-1;
	j = j-1;
	
	matriz[i].push_back(PAR(j,0.0));
	
}

void Esparsa::a_semi_estocastica(){
	
	double proba;
	
	for(int i = 0; i < tam; i++){
		
		proba = (double) matriz[i].size();
		proba = 1.0 / proba;
		intermedio[i] = 0.0;
		
		for (ITERADOR it=matriz[i].begin(); it != matriz[i].end(); ++it){
				
			(*it).second = proba;
				
		}
		
	}
	
}

/* Solo acepta memoria dinamica */
void Esparsa::mul_vector(double*& y, double* x, int n){
	
	double* y_nuevo = intermedio;
	
	if(n == tam){
		
		for(int i = 0; i < n; i++){
			
			for (ITERADOR it=matriz[i].begin(); it != matriz[i].end(); ++it){
				
				y_nuevo[(*it).first] += x[i] * (*it).second;
				
			}
			
			//reciclo ciclo: pongo en 0 un arreglo.
			y[i] = 0.0;
			
		}
		
		intermedio = y;
		y = y_nuevo;
		
	}else{
		
		MENSAJE("ERROR: Dimensiones no coinciden.");
		while(1);
		
	}
	
}
